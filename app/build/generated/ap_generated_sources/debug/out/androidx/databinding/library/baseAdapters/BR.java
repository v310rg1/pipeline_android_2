package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int areElementsVisible = 1;

  public static final int dictionaryCount = 2;

  public static final int dictionaryMax = 3;

  public static final int flower = 4;

  public static final int isDictionaryLoaded = 5;

  public static final int isLoading = 6;

  public static final int isLogin = 7;

  public static final int loadingBehaviours = 8;
}

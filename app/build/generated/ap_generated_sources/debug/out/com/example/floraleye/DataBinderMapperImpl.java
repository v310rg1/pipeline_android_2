package com.example.floraleye;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.example.floraleye.databinding.ActivityEditProfileBindingImpl;
import com.example.floraleye.databinding.FragmentDictionaryBindingImpl;
import com.example.floraleye.databinding.FragmentLoadingBindingImpl;
import com.example.floraleye.databinding.FragmentOnboardBindingImpl;
import com.example.floraleye.databinding.FragmentProfileBindingImpl;
import com.example.floraleye.databinding.FragmentQuizBindingImpl;
import com.example.floraleye.databinding.ItemDictionaryflowerListBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYEDITPROFILE = 1;

  private static final int LAYOUT_FRAGMENTDICTIONARY = 2;

  private static final int LAYOUT_FRAGMENTLOADING = 3;

  private static final int LAYOUT_FRAGMENTONBOARD = 4;

  private static final int LAYOUT_FRAGMENTPROFILE = 5;

  private static final int LAYOUT_FRAGMENTQUIZ = 6;

  private static final int LAYOUT_ITEMDICTIONARYFLOWERLIST = 7;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(7);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.floraleye.R.layout.activity_edit_profile, LAYOUT_ACTIVITYEDITPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.floraleye.R.layout.fragment_dictionary, LAYOUT_FRAGMENTDICTIONARY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.floraleye.R.layout.fragment_loading, LAYOUT_FRAGMENTLOADING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.floraleye.R.layout.fragment_onboard, LAYOUT_FRAGMENTONBOARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.floraleye.R.layout.fragment_profile, LAYOUT_FRAGMENTPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.floraleye.R.layout.fragment_quiz, LAYOUT_FRAGMENTQUIZ);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.floraleye.R.layout.item_dictionaryflower_list, LAYOUT_ITEMDICTIONARYFLOWERLIST);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYEDITPROFILE: {
          if ("layout/activity_edit_profile_0".equals(tag)) {
            return new ActivityEditProfileBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_edit_profile is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTDICTIONARY: {
          if ("layout/fragment_dictionary_0".equals(tag)) {
            return new FragmentDictionaryBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_dictionary is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTLOADING: {
          if ("layout/fragment_loading_0".equals(tag)) {
            return new FragmentLoadingBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_loading is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTONBOARD: {
          if ("layout/fragment_onboard_0".equals(tag)) {
            return new FragmentOnboardBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_onboard is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPROFILE: {
          if ("layout/fragment_profile_0".equals(tag)) {
            return new FragmentProfileBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_profile is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTQUIZ: {
          if ("layout/fragment_quiz_0".equals(tag)) {
            return new FragmentQuizBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_quiz is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMDICTIONARYFLOWERLIST: {
          if ("layout/item_dictionaryflower_list_0".equals(tag)) {
            return new ItemDictionaryflowerListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_dictionaryflower_list is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(9);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "areElementsVisible");
      sKeys.put(2, "dictionaryCount");
      sKeys.put(3, "dictionaryMax");
      sKeys.put(4, "flower");
      sKeys.put(5, "isDictionaryLoaded");
      sKeys.put(6, "isLoading");
      sKeys.put(7, "isLogin");
      sKeys.put(8, "loadingBehaviours");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(7);

    static {
      sKeys.put("layout/activity_edit_profile_0", com.example.floraleye.R.layout.activity_edit_profile);
      sKeys.put("layout/fragment_dictionary_0", com.example.floraleye.R.layout.fragment_dictionary);
      sKeys.put("layout/fragment_loading_0", com.example.floraleye.R.layout.fragment_loading);
      sKeys.put("layout/fragment_onboard_0", com.example.floraleye.R.layout.fragment_onboard);
      sKeys.put("layout/fragment_profile_0", com.example.floraleye.R.layout.fragment_profile);
      sKeys.put("layout/fragment_quiz_0", com.example.floraleye.R.layout.fragment_quiz);
      sKeys.put("layout/item_dictionaryflower_list_0", com.example.floraleye.R.layout.item_dictionaryflower_list);
    }
  }
}

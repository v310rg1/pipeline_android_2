package com.example.floraleye.databinding;
import com.example.floraleye.R;
import com.example.floraleye.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOnboardBindingImpl extends FragmentOnboardBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.flowerIcon, 4);
        sViewsWithIds.put(R.id.mailPasswordConstraintLayout, 5);
        sViewsWithIds.put(R.id.inputLayoutMail, 6);
        sViewsWithIds.put(R.id.inputEditTextMail, 7);
        sViewsWithIds.put(R.id.inputLayoutPassword, 8);
        sViewsWithIds.put(R.id.inputEditTextPassword, 9);
        sViewsWithIds.put(R.id.inputEditTextRepeatPassword, 10);
        sViewsWithIds.put(R.id.bottom_guideline, 11);
        sViewsWithIds.put(R.id.onboardProgressBar, 12);
        sViewsWithIds.put(R.id.switchModeTextView, 13);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentOnboardBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 14, sIncludes, sViewsWithIds));
    }
    private FragmentOnboardBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.Guideline) bindings[11]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (com.google.android.material.textfield.TextInputEditText) bindings[7]
            , (com.google.android.material.textfield.TextInputEditText) bindings[9]
            , (com.google.android.material.textfield.TextInputEditText) bindings[10]
            , (com.google.android.material.textfield.TextInputLayout) bindings[6]
            , (com.google.android.material.textfield.TextInputLayout) bindings[8]
            , (com.google.android.material.textfield.TextInputLayout) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (com.google.android.material.button.MaterialButton) bindings[3]
            , (android.widget.ProgressBar) bindings[12]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[13]
            );
        this.forgotPasswordTextView.setTag(null);
        this.inputLayoutRepeatPassword.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.onboardButton.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.isLogin == variableId) {
            setIsLogin((java.lang.Boolean) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setIsLogin(@Nullable java.lang.Boolean IsLogin) {
        this.mIsLogin = IsLogin;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.isLogin);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int isLoginViewGONEViewVISIBLE = 0;
        java.lang.Boolean isLogin = mIsLogin;
        int isLoginViewVISIBLEViewGONE = 0;
        java.lang.String isLoginOnboardButtonAndroidStringStrLoginButtonOnboardButtonAndroidStringStrSignupButton = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxIsLogin = false;

        if ((dirtyFlags & 0x3L) != 0) {



                // read androidx.databinding.ViewDataBinding.safeUnbox(isLogin)
                androidxDatabindingViewDataBindingSafeUnboxIsLogin = androidx.databinding.ViewDataBinding.safeUnbox(isLogin);
            if((dirtyFlags & 0x3L) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxIsLogin) {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x20L;
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x4L;
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x40L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(isLogin) ? View.GONE : View.VISIBLE
                isLoginViewGONEViewVISIBLE = ((androidxDatabindingViewDataBindingSafeUnboxIsLogin) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                // read androidx.databinding.ViewDataBinding.safeUnbox(isLogin) ? View.VISIBLE : View.GONE
                isLoginViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxIsLogin) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read androidx.databinding.ViewDataBinding.safeUnbox(isLogin) ? @android:string/str_login_button : @android:string/str_signup_button
                isLoginOnboardButtonAndroidStringStrLoginButtonOnboardButtonAndroidStringStrSignupButton = ((androidxDatabindingViewDataBindingSafeUnboxIsLogin) ? (onboardButton.getResources().getString(R.string.str_login_button)) : (onboardButton.getResources().getString(R.string.str_signup_button)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.forgotPasswordTextView.setVisibility(isLoginViewVISIBLEViewGONE);
            this.inputLayoutRepeatPassword.setVisibility(isLoginViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.onboardButton, isLoginOnboardButtonAndroidStringStrLoginButtonOnboardButtonAndroidStringStrSignupButton);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): isLogin
        flag 1 (0x2L): null
        flag 2 (0x3L): androidx.databinding.ViewDataBinding.safeUnbox(isLogin) ? View.GONE : View.VISIBLE
        flag 3 (0x4L): androidx.databinding.ViewDataBinding.safeUnbox(isLogin) ? View.GONE : View.VISIBLE
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(isLogin) ? View.VISIBLE : View.GONE
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(isLogin) ? View.VISIBLE : View.GONE
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(isLogin) ? @android:string/str_login_button : @android:string/str_signup_button
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(isLogin) ? @android:string/str_login_button : @android:string/str_signup_button
    flag mapping end*/
    //end
}
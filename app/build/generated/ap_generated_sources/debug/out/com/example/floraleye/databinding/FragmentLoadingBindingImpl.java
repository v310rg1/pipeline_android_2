package com.example.floraleye.databinding;
import com.example.floraleye.R;
import com.example.floraleye.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentLoadingBindingImpl extends FragmentLoadingBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.progressBar, 2);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentLoadingBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private FragmentLoadingBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ProgressBar) bindings[2]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.loadingBehaviours == variableId) {
            setLoadingBehaviours((com.example.floraleye.ui.onboard.LoadingFragment.LoadingBehaviours) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setLoadingBehaviours(@Nullable com.example.floraleye.ui.onboard.LoadingFragment.LoadingBehaviours LoadingBehaviours) {
        this.mLoadingBehaviours = LoadingBehaviours;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.loadingBehaviours);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.floraleye.ui.onboard.LoadingFragment.LoadingBehaviours loadingBehaviours = mLoadingBehaviours;
        boolean loadingBehavioursLoadingBehavioursCHECKONBOARD = false;
        java.lang.String loadingBehavioursLoadingBehavioursCHECKONBOARDMboundView1AndroidStringStrCheckOnboardMboundView1AndroidStringStrOnboardSuccess = null;

        if ((dirtyFlags & 0x3L) != 0) {



                // read loadingBehaviours == LoadingBehaviours.CHECK_ONBOARD
                loadingBehavioursLoadingBehavioursCHECKONBOARD = (loadingBehaviours) == (com.example.floraleye.ui.onboard.LoadingFragment.LoadingBehaviours.CHECK_ONBOARD);
            if((dirtyFlags & 0x3L) != 0) {
                if(loadingBehavioursLoadingBehavioursCHECKONBOARD) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read loadingBehaviours == LoadingBehaviours.CHECK_ONBOARD ? @android:string/str_check_onboard : @android:string/str_onboard_success
                loadingBehavioursLoadingBehavioursCHECKONBOARDMboundView1AndroidStringStrCheckOnboardMboundView1AndroidStringStrOnboardSuccess = ((loadingBehavioursLoadingBehavioursCHECKONBOARD) ? (mboundView1.getResources().getString(R.string.str_check_onboard)) : (mboundView1.getResources().getString(R.string.str_onboard_success)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, loadingBehavioursLoadingBehavioursCHECKONBOARDMboundView1AndroidStringStrCheckOnboardMboundView1AndroidStringStrOnboardSuccess);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): loadingBehaviours
        flag 1 (0x2L): null
        flag 2 (0x3L): loadingBehaviours == LoadingBehaviours.CHECK_ONBOARD ? @android:string/str_check_onboard : @android:string/str_onboard_success
        flag 3 (0x4L): loadingBehaviours == LoadingBehaviours.CHECK_ONBOARD ? @android:string/str_check_onboard : @android:string/str_onboard_success
    flag mapping end*/
    //end
}
package com.example.floraleye.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Classe per i fiori del Dizionario.
 */
class DictionaryFlower(scientificName: String,
                       commonName: String,
                       imageURL: String,
                       description: String,
                       taxonomy: Taxonomy
): Parcelable {
    /**
     * Nome Scientifico di un Fiore.
     */
    var scientificName: String
        private set
    /**
     * Nome Comune di un fiore.
     */
    var commonName: String
        private set
    /**
     * URL dell'immagine di un fiore.
     */
    var imageURL : String
        private set
    /**
     * Descrizione di un fiore.
     */
    var description : String
        private set

    /**
     * Tassonomia di un fiore.
     */
    var taxonomy: Taxonomy
        private set

    constructor(parcel: Parcel) : this(
        scientificName = parcel.readString().toString(),
        commonName = parcel.readString().toString(),
        imageURL = parcel.readString().toString(),
        description = parcel.readString().toString(),
        taxonomy = parcel.readParcelable<Taxonomy>(Taxonomy::class.java.classLoader) as Taxonomy
    )

    init {
        this.scientificName = scientificName
        this.commonName = commonName
        this.imageURL = imageURL
        this.description = description
        this.taxonomy = taxonomy
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(scientificName)
        parcel.writeString(commonName)
        parcel.writeString(imageURL)
        parcel.writeString(description)
        parcel.writeParcelable(taxonomy, 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    /**
     * Parcelable CREATOR della classe DictionaryFlower.
     */
    companion object CREATOR : Parcelable.Creator<DictionaryFlower> {
        override fun createFromParcel(parcel: Parcel): DictionaryFlower {
            return DictionaryFlower(parcel)
        }

        override fun newArray(size: Int): Array<DictionaryFlower?> {
            return arrayOfNulls(size)
        }
    }
}

package com.example.floraleye.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Classe per la gestione della tassonomia di un Fiore.
 */
class Taxonomy(
    kingdom: String,
    phylum: String,
    order: String,
    family: String,
    genus: String,
    species: String
): Parcelable {
    /**
     * Regno di un fiore.
     */
    var kingdom: String
        private set
    /**
     * Phylum di un fiore.
     */
    var phylum: String
        private set
    /**
     * Ordine di un fiore.
     */
    var order: String
        private set
    /**
     * Famiglia di un fiore.
     */
    var family: String
        private set
    /**
     * Genere di un fiore.
     */
    var genus: String
        private set
    /**
     * Specie di un fiore.
     */
    var species: String
        private set

    constructor(parcel: Parcel) : this(
        kingdom = parcel.readString().toString(),
        phylum = parcel.readString().toString(),
        order = parcel.readString().toString(),
        family = parcel.readString().toString(),
        genus = parcel.readString().toString(),
        species = parcel.readString().toString()
    )

    init{
        this.kingdom = kingdom
        this.phylum = phylum
        this.order = order
        this.family = family
        this.genus = genus
        this.species = species
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(kingdom)
        parcel.writeString(phylum)
        parcel.writeString(order)
        parcel.writeString(family)
        parcel.writeString(genus)
        parcel.writeString(species)
    }

    override fun describeContents(): Int {
        return 0
    }

    /**
     * Parcelable CREATOR della classe Taxonomy.
     */
    companion object CREATOR : Parcelable.Creator<Taxonomy> {
        override fun createFromParcel(parcel: Parcel): Taxonomy {
            return Taxonomy(parcel)
        }

        override fun newArray(size: Int): Array<Taxonomy?> {
            return arrayOfNulls(size)
        }
    }
}

package com.example.floraleye.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.floraleye.repositories.DictionaryRepository

/**
 * Classe di factory da utilizzare per la costruzione del DictionaryViewModel.
 */
class DictionaryViewModelFactory(
    dictionaryRepository: DictionaryRepository
) : ViewModelProvider.Factory {

    private val dictionaryRepository: DictionaryRepository

    init {
        this.dictionaryRepository = dictionaryRepository
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DictionaryViewModel(dictionaryRepository) as T
    }

}

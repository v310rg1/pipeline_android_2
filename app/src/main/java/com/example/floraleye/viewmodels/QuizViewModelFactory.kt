package com.example.floraleye.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.floraleye.repositories.IQuizRepository
import com.example.floraleye.utils.Constants

/**
 * Classe di factory da utilizzare per la costruzione del QuizViewModel.
 */
class QuizViewModelFactory (
    private val application: Application,
    quizRepository: IQuizRepository
) : ViewModelProvider.Factory {

    private val quizRepository: IQuizRepository

    init {
        this.quizRepository = quizRepository
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(QuizViewModel::class.java)) {
            return QuizViewModel(application, quizRepository) as T
        }
        throw IllegalArgumentException(Constants.UNKNOWN_VIEWMODEL)
    }
}

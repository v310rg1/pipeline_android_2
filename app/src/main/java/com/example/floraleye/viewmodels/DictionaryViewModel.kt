package com.example.floraleye.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.floraleye.models.DictionaryFlower
import com.example.floraleye.repositories.DictionaryRepository

/**
 * View Model per la gestione del Dizionario.
 */
class DictionaryViewModel(
    private val repository: DictionaryRepository
) : ViewModel() {

    /**
     * Variabile che controlla se i fiori sono stati inizializzati nel Dizionario.
     */
    val bIsFlowerListInitialized: MutableLiveData<Boolean>
        get() = repository.bIsFlowerListInitialized

    /**
     *  Contatore per il download dei fiori.
     */
    val repoDownloadCounter: MutableLiveData<String>
        get() = repository.counter

    /**
     * Variabile che controlla la presenza di errori durante il download dei dati dei fiori.
     */
    val bShowErrorMessage: MutableLiveData<Boolean>
        get() = repository.bShowErrorMessage

    /**
     * Metodo per ottenere la lista dei fiori inizializzati nel Dizionario.
     * @return MutableLiveData<MutableList<DictionaryFlower>> lista di fiori del Dizionario
     */
    fun getDictionaryList(): MutableLiveData<MutableList<DictionaryFlower>> {
        return repository.flowersList
    }

    /**
     * Metodo per l'inizializzazione dei dati nel Dizionario.
     */
    fun initializeFlowersList(){
        repository.initializeFlowersList()
    }

    /**
     * Metodo che ordina la lista di fiori del Dizionario in base al loro nome scientifico dalla A alla Z.
     */
    fun sortDictionaryEntries(){
        repository.sortDictionaryEntries()
    }

    /**
     * Metodo per controllare se il thread che ottiene i dati per il Dizionario è partito.
     * @return Boolean indica se il thread per il parsing dei dati del dizionario è partito o no
     */
    fun isLoadingDictionary() : Boolean {
        return repository.isThreadStarted
    }

    /**
     * Metodo per effettuare il clean della repository.
     */
    fun cleanRepository(){
        repository.clean()
    }

    /**
     * Metodo per caricare il dizionario da un file JSON contenente oggetti DictionaryFlower.
     */
    fun loadFromJSON(context: Context){
        repository.loadDictionaryFromJSON(context)
    }

    /**
     * Metodo per ottenere la dimensione della lista dei fiori da ricercare per il dizionario.
     */
    fun getFlowersNameListSize() : Int {
        return repository.flowersName.size
    }

    /**
     * Metodo per controllare se il dizionario è stato salvato nella cache.
     * @return Boolean indica se il dizionario è stato salvato nella cache.
     */
    fun isDictionaryCached(context : Context) : Boolean {
        return repository.isDictionaryCached(context)
    }

    /**
     * Metodo per effettuare la cache del dizionario.
     * Return Boolean indica se è stata effettuata la cache del dizionario.
     */
    fun cacheDictionary(context : Context): Boolean{
        return repository.cacheDictionary(context)
    }
}

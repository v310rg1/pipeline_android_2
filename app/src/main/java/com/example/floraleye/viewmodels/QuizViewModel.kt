package com.example.floraleye.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.floraleye.models.Quiz
import com.example.floraleye.repositories.IQuizRepository

/**
 * ViewModel per la gestione delle funzionalità relative alla sottoposizione di quiz
 * agli utenti.
 */
class QuizViewModel (
    application: Application,
    private val quizRepository: IQuizRepository
) : AndroidViewModel(application) {

    /**
     * Variabile che contiene come Live Data la lista di quiz da sottoporre agli utenti.
     * Viene utilizzata per salvare lo stato della lista di quiz in QuizFragment.
     */
    private var mQuizzes: MutableLiveData<List<Quiz>> = MutableLiveData()

    init {
        // Al momento dell'inizializzazione viene caricata la lista di quiz tramite QuizRepository.
        loadQuizzes()
    }

    /**
     * Metodo utilizzato per recuperare la lista di quiz da sottoporre agli utenti.
     * @return LiveData della lista di quiz.
     */
    fun getQuizzes(): LiveData<List<Quiz>> {
        return mQuizzes
    }

    /**
     * Metodo utilizzato per caricare la lista di quiz da Firebase Realtime Database tramite
     * QuizRepository.
     */
    private fun loadQuizzes() {
        mQuizzes = quizRepository.getQuizzesMutableLiveData()
    }

    /**
     * Metodo per controllare se il thread che ottiene la lista di quiz è partito.
     * @return Boolean indica se il thread per il caricamento dei quiz è partito.
     */
    fun isLoadingQuizzes() : Boolean {
        return quizRepository.isThreadStarted
    }

}

package com.example.floraleye.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.floraleye.R
import com.example.floraleye.databinding.FragmentProfileBinding
import com.example.floraleye.repositories.UserRepository
import com.example.floraleye.utils.GeneralUtils
import com.example.floraleye.utils.OnboardUtils
import com.example.floraleye.viewmodels.UserViewModel
import com.example.floraleye.viewmodels.UserViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder


/**
 * Fragment per la gestione del Profilo.
 */
class ProfileFragment : Fragment() {

    private lateinit var mBinding: FragmentProfileBinding

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentProfileBinding.inflate(inflater, container, false)

        userViewModel = ViewModelProvider(
            this,
            UserViewModelFactory(requireActivity().application,
                UserRepository(requireActivity().application))
        )[UserViewModel::class.java]

        mBinding.mailTextView.text =
            userViewModel.userMail ?: resources.getString(R.string.str_no_user)

        mBinding.deleteUserButton.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.str_warning)
                .setMessage(R.string.str_delete_user_confirmation)
                .setPositiveButton(R.string.str_ok) { _, _ ->
                    deleteCurrentUser()
                }
                .setNeutralButton(R.string.str_cancel, null)
                .setCancelable(true)
                .show()
        }

        return mBinding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.toolbar_menu_profile, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_logout -> {
                userViewModel.logout()
            }
            R.id.item_edit_profile -> {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage(R.string.str_edit_profile_message)
                    .setPositiveButton(R.string.str_ok) { _, _ ->
                        val intent = Intent(
                            activity,
                            EditProfileActivity::class.java
                        )
                        startActivity(intent)
                    }
                    .setNeutralButton(R.string.str_cancel, null)
                    .setCancelable(true)
                    .show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteCurrentUser() {
        mBinding.deleteUserButton.isEnabled = false
        mBinding.deleteUserProgressBar.visibility = View.VISIBLE

        userViewModel.deleteUser().observe(viewLifecycleOwner) {result ->
            mBinding.deleteUserButton.isEnabled = true
            mBinding.deleteUserProgressBar.visibility = View.INVISIBLE

            val resultMessage =
                OnboardUtils.getOnboardResultErrorMessage(result, resources) ?: return@observe

            GeneralUtils.showGenericMessageOnView(
                message = resultMessage,
                context = requireContext(),
                root = mBinding.root,
                anchorView = mBinding.deleteUserButton
            )
        }
    }
}

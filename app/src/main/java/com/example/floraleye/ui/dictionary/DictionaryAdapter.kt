package com.example.floraleye.ui.dictionary

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.floraleye.R
import com.example.floraleye.databinding.ItemDictionaryflowerListBinding
import com.example.floraleye.models.DictionaryFlower
import com.example.floraleye.viewmodels.DictionaryViewModel

/**
 * Adapter per la gestione del dizionario.
 */
class DictionaryAdapter(
    private val viewModel: DictionaryViewModel,
) :
    RecyclerView.Adapter<DictionaryAdapter.FlowersViewHolder>() {

    private lateinit var mBinding: ItemDictionaryflowerListBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlowersViewHolder {
        mBinding = ItemDictionaryflowerListBinding.inflate(
            LayoutInflater.from(parent.context), parent, false)
        return FlowersViewHolder(mBinding)
    }

    override fun onBindViewHolder(holder: FlowersViewHolder, position: Int) {

        val flowerList = viewModel.getDictionaryList().value

        if (flowerList != null){
            holder.bindFlower(flowerList[position])
        }
    }

    override fun getItemCount(): Int {

        val currentList = viewModel.getDictionaryList().value

        if (currentList != null){
            return currentList.size
        }

        return 0
    }

    /**
     * ViewHolder per l'inizializzazione dei dati del dizionario.
     */
    class FlowersViewHolder(mBinding: ItemDictionaryflowerListBinding): RecyclerView.ViewHolder(
        mBinding.root
    ) {

        private val mBinding : ItemDictionaryflowerListBinding = mBinding

        /**
         * Metodo per l'inizializzazione di ogni singolo item Fiore nel dizionario.
         * @param flower Il fiore nel dizionario da inizializzare
         */
        fun bindFlower(flower: DictionaryFlower) {

            Glide
                .with(mBinding.flowerImage.context)
                .load(flower.imageURL)
                .centerCrop()
                .placeholder(R.drawable.ic_dictionary_image_placeholder)
                .into(mBinding.flowerImage);

            mBinding.flower = flower
        }
    }
}




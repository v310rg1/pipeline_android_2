package com.example.floraleye.ui.dictionary;

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.floraleye.R
import com.example.floraleye.databinding.FragmentDictionaryBinding
import com.example.floraleye.repositories.DictionaryRepository
import com.example.floraleye.utils.Constants.DICTIONARY_CACHE_FILE
import com.example.floraleye.viewmodels.DictionaryViewModel
import com.example.floraleye.viewmodels.DictionaryViewModelFactory
import com.google.android.material.snackbar.Snackbar
import java.io.File

/**
 * Fragment per la gestione del Dizionario.
 */
public class DictionaryFragment : Fragment() {

    private lateinit var mBinding: FragmentDictionaryBinding
    private lateinit var dictionaryViewModel: DictionaryViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        mBinding = FragmentDictionaryBinding.inflate(inflater, container, false)

        dictionaryViewModel = ViewModelProvider(
            this,
            DictionaryViewModelFactory(DictionaryRepository())
        )[DictionaryViewModel::class.java]

        initDictionary()

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dictionaryViewModel.bIsFlowerListInitialized.observe(this){

            if (mBinding.isDictionaryLoaded == true){
                mBinding.rvFlowerList.adapter = DictionaryAdapter(dictionaryViewModel)
            }
        }

        mBinding.swipeRefreshDictionary.setOnRefreshListener {
            onRefresh()
            mBinding.swipeRefreshDictionary.isRefreshing = false
        }
    }

    private fun onRefresh(){
        //Log.d("Dictionary", "Refreshing Called!")

        if(!dictionaryViewModel.isLoadingDictionary()) {

            dictionaryViewModel.cleanRepository()
            mBinding.rvFlowerList.adapter?.notifyDataSetChanged()

            File(context?.cacheDir, DICTIONARY_CACHE_FILE).delete()
            initDictionary()
        }
    }

    private fun initDictionary(){

        initializeDictionaryObservers()

        if (dictionaryViewModel.bIsFlowerListInitialized.value == false){

            if (dictionaryViewModel.isDictionaryCached(context as Context)){
                dictionaryViewModel.loadFromJSON(context as Context)
            }
            else {
                if (!dictionaryViewModel.isLoadingDictionary()) {
                    dictionaryViewModel.initializeFlowersList()
                }
            }
        }
    }

    private fun initializeDictionaryObservers(){

        dictionaryViewModel.bIsFlowerListInitialized.observe(this){

            mBinding.isDictionaryLoaded = it
            if (mBinding.isDictionaryLoaded == false){
                mBinding.dictionaryProgressBar.visibility = View.VISIBLE
                mBinding.dictionaryDownloadIndicator.visibility = View.VISIBLE
                mBinding.rvFlowerList.visibility = View.INVISIBLE
            }
            else {
                dictionaryViewModel.sortDictionaryEntries()
                mBinding.dictionaryProgressBar.visibility = View.INVISIBLE
                mBinding.dictionaryDownloadIndicator.visibility = View.INVISIBLE
                mBinding.rvFlowerList.visibility = View.VISIBLE
                mBinding.rvFlowerList.adapter?.notifyDataSetChanged()

                if (!dictionaryViewModel.isDictionaryCached(context as Context))
                    dictionaryViewModel.cacheDictionary(context as Context)
            }
        }

        dictionaryViewModel.repoDownloadCounter.observe(this){ counter ->
            mBinding.dictionaryCount = counter
            mBinding.dictionaryMax = dictionaryViewModel.getFlowersNameListSize().toString()
        }

        dictionaryViewModel.bShowErrorMessage.observe(this){ error ->
            if (error == true){
                Snackbar.make(mBinding.root,
                    R.string.str_snack_notification_dictionary,
                    Snackbar.LENGTH_LONG)
                    .setAnchorView(mBinding.dictionaryDownloadIndicator)
                    .show()
                dictionaryViewModel.bShowErrorMessage.value = false
            }
        }
    }
}

package com.example.floraleye.utils

import android.content.Context
import android.view.View
import com.example.floraleye.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar


/**
 * Funzioni di utilità di uso generale.
 */
object GeneralUtils {

    /**
     * Metodo per mostrare un messaggio generico.
     * @param message messaggio da mostrare
     * @param context contesto di riferimento
     * @param root root della view di riferimento
     * @param anchorView view a cui ancorare la snackbar
     */
    fun showGenericMessageOnView(message: String,
                                 context: Context,
                                 root: View,
                                 anchorView: View
    )  {
        if (message.length > Constants.MESSAGE_LENGTH_FOR_SNACKBAR_LIMIT) {
            MaterialAlertDialogBuilder(context)
                .setMessage(message)
                .setNeutralButton(R.string.str_ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .setCancelable(true)
                .show()
        } else {
            Snackbar.make(root,
                message,
                Snackbar.LENGTH_LONG)
                .setAnchorView(anchorView)
                .show()
        }
    }

    /**
     * Metodo utilizzare per mostrare un messaggio di errore nel caso di password specificata non
     * sicura.
     * @param context Contesto di riferimento
     */
    fun showPasswordSuggestionMessage(context: Context) {
        MaterialAlertDialogBuilder(context)
            .setTitle(R.string.str_warning)
            .setMessage(R.string.str_password_not_valid)
            .setNeutralButton(R.string.str_ok) { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(true)
            .show()
    }
}

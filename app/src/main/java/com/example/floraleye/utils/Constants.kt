package com.example.floraleye.utils


/**
 * Oggetto contenente tutte le costanti utilizzate.
 */
object Constants {

    // *** inizio regione - firebase

    /**
     * URL del Realtime Database di Firebase utilizzato per l'applicazione.
     */
    const val FIREBASE_REALTIME_DB =
        "https://floraleye-default-rtdb.europe-west1.firebasedatabase.app/"

    /**
     * Codice di stato ritornato nel caso di registrazione/onboard concluso con successo.
     */
    const val FIREBASE_AUTH_OK = 200

    /**
     * Codice di stato ritornato se la mail per il ripristino della password è stata inviata con
     * successo all'indirizzo e-mail specificato dall'utente.
     */
    const val FIREBASE_AUTH_SEND_RESET_PASSWORD_OK = 201

    /**
     * Codice di stato ritornato nel caso di errore generico in fase di registrazione/onboard.
     */
    const val FIREBASE_AUTH_GENERIC_ERROR = 400

    /**
     * Codice di stato ritornato nel caso di tentativo di registrazione con mail già in uso da
     * un altro utente.
     */
    const val FIREBASE_SIGNUP_USER_EXISTS_ERROR = 401

    /**
     * Codice di stato ritornato nel caso di tentativo di registrazione/onboard con password troppo debole
     * per i criteri di sicurezza di Firebase (NON quelli dell'app).
     */
    const val FIREBASE_AUTH_WEAK_PASSWORD_ERROR = 402

    /**
     * Codice di stato ritornato nel caso di tentativo di registrazione/onboard con e-mail malformata.
     */
    const val FIREBASE_AUTH_CREDENTIAL_ERROR = 403

    /**
     * Codice di stato ritornato nel caso di tentativo di registrazione/onboard con connessione
     * assente o lenta.
     */
    const val FIREBASE_AUTH_NETWORK_ERROR = 404

    /**
     * Codice di stato ritornato nel caso di login con indirizzo e-mail non associato ad alcun
     * utente precedentemente registrato.
     */
    const val FIREBASE_AUTH_INVALID_USER_ERROR = 405

    /**
     * Codice di stato ritornato nel caso di tentativo di login con indirizzo e-mail non ancora
     * verificato.
     */
    const val FIREBASE_AUTH_MAIL_NOT_VERIFIED = 406

    /**
     * Codice di stato ritornato nel caso si facciano troppe richieste a Firebase Auth in un tempo
     * limitato.
     */
    const val FIREBASE_AUTH_TOO_MANY_REQUEST = 407

    /**
     * Codice di stato ritornato nel caso di tentativo di eliminazione dell'account di un utente
     * con una autenticazione troppo vecchia.
     */
    const val FIREBASE_AUTH_LOGIN_TOO_OLD = 408

    // *** fine regione - firebase

    // *** inizio regione - quiz

    /**
     * Nome del database utilizzato per memorizzare la lista di singoli quiz
     * da sottoporre agli utenti.
     */
    const val QUIZZES = "quizzes"

    /**
     * Stringa rappresentante l'identificatore di un quiz.
     */
    const val IDENTIFIER = "id"

    /**
     * Stringa rappresentante la domanda di un quiz.
     */
    const val QUESTION = "question"

    /**
     * Stringa rappresentante l'immagine di un quiz.
     */
    const val IMAGE = "image"

    /**
     * Stringa rappresentante la soluzione di un quiz.
     */
    const val SOLUTION = "solution"

    /**
     * Stringa rappresentante la prima risposta disponibile per un quiz.
     */
    const val ANSWER1 = "answer1"

    /**
     * Stringa rappresentante la seconda risposta disponibile per un quiz.
     */
    const val ANSWER2 = "answer2"

    /**
     * Stringa rappresentante la terza risposta disponibile per un quiz.
     */
    const val ANSWER3 = "answer3"

    /**
     * Stringa rappresentante la quarta risposta disponibile per un quiz.
     */
    const val ANSWER4 = "answer4"

    /**
     * Messaggio di errore per il mancato recupero dei quiz da Firebase Realtime Database.
     */
    const val FAILED_READ = "Failed to read value."

    /**
     * Stringa utilizzata per poter recuperare il quiz corretto
     * in caso di cambio di configurazione.
     */
    const val QUIZ_INDEX = "QUIZ_INDEX"

    /**
     * Stringa utilizzata per poter recuperare la soluzione di un quiz
     * in caso di cambio di configurazione.
     */
    const val QUIZ_SOLUTION = "QUIZ_SOLUTION"

    /**
     * Stringa utilizzata per poter memorizzare il fatto che la risposta al quiz
     * sia già stata sottomessa e tenerne conto in caso di cambio di configurazione.
     */
    const val SUBMIT_BUTTON_CLICKED = "SUBMIT_BUTTON_CLICKED"

    /**
     * Indice del primo radio button.
     */
    const val INDEX_RADIO_BUTTON1 = 0

    /**
     * Indice del secondo radio button.
     */
    const val INDEX_RADIO_BUTTON2 = 1

    /**
     * Indice del terzo radio button.
     */
    const val INDEX_RADIO_BUTTON3 = 2

    /**
     * Indice del quarto radio button.
     */
    const val INDEX_RADIO_BUTTON4 = 3

    /**
     * Valore iniziale dell'animazione dell'opacità del background delle risposte.
     */
    const val START_OPACITY = 0.1f

    /**
     * Valore finale dell'animazione dell'opacità del background delle risposte.
     */
    const val END_OPACITY = 1.0f

    /**
     * Durata dell'animazione di opacità del background delle risposte in millisecondi.
     */
    const val ANSWER_ANIMATION_DURATION = 500

    /**
     * Durata dell'animazione del bottone Next.
     */
    const val NEXT_BUTTON_ANIMATION_DURATION = 1000

    /**
     * Ampiezza della colorazione del bordo della risposta corretta e della eventuale
     * risposta errata.
     */
    const val ANSWER_STROKE_WIDTH = 8

    // *** fine regione - quiz


    // *** inizio regione - generale

    /**
     * Lunghezza massima per i messaggi presentabili tramite Snackbar. Se i messaggio eccede questa
     * lunghezza dovrà essere mostrato in altro modo, ad esempio con un pop up.
     */
    const val MESSAGE_LENGTH_FOR_SNACKBAR_LIMIT = 60

    /**
     * Messaggio di eccezione per il tentativo di creare un ViewModel di una classe sconosciuta.
     */
    const val UNKNOWN_VIEWMODEL = "Unknown ViewModel class"

    // *** fine regione - generale


    // *** inizio regione - dizionario

    /**
     * Endpoint per le richieste all'API GBIF.
     */
    const val GBIF_ENDPOINT = "/v1/species/match"

    /**
     * Endpoint per le richieste all'API Wikipedia.
     */
    const val WIKI_ENDPOINT = "/w/api.php"

    /**
     * URL dell'API GBIF.
     */
    const val GBIF_API_URL = "https://api.gbif.org/"

    /**
    * URL dell'API GBIF.
    */
    const val WIKI_API_URL = "https://en.wikipedia.org/"

    /**
     * Nome del file JSON per il caching del Dizionario.
     */
    const val DICTIONARY_CACHE_FILE = "dictionary.json"

    /**
     * Stringa iniziale per il contatore download Dizionario.
     */
    const val DICTIONARY_COUNTER_START = "0"

    // *** fine regione - dizionario
}

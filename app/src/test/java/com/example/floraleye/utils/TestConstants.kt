package com.example.floraleye.utils

object TestConstants {

    // *** inizio regione - Quiz

    const val IDENTIFIER = "id"

    const val QUESTION = "question"

    const val IMAGE = "image"

    const val SOLUTION = "solution"

    const val ANSWER0 = "answer0"

    const val ANSWER1 = "answer1"

    const val ANSWER2 = "answer2"

    const val ANSWER3 = "answer3"

    const val ANSWER4 = "answer4"

    const val QUIZREPOSITORY_IDENTIFIER = "1"

    const val QUIZREPOSITORY_IMAGE = "https://firebasestorage.googleapis.com/v0/b/" +
            "floraleye.appspot.com/o/quiz1%2Fimage_01307.jpg?" +
            "alt=media&token=0dbfe1af-624b-40c9-bd20-2b702cb5c317"

    const val QUIZREPOSITORY_QUESTION = "What is the genus of flower represented in this image?"

    const val QUIZREPOSITORY_ANSWER1 = "Rose"

    const val QUIZREPOSITORY_ANSWER2 = "Azalea"

    const val QUIZREPOSITORY_ANSWER3 = "Camellia"

    const val QUIZREPOSITORY_ANSWER4 = "Primula"

    const val QUIZREPOSITORY_SOLUTION = "Rose"

    const val FAILED_READ = "Failed to read value."

    const val ASSERTS_NOT_SUCCESSFUL = "Asserts are not successful."

    // *** fine regione - Quiz

}

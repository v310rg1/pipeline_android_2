package com.example.floraleye

object TestConstants {

    const val TEST_ACCOUNT_EMAIL = "floraleye.test@gmail.com"

    const val TEST_ACCOUNT_PASSWORD = "Floraleye2023!"

    const val TEST_ACCOUNT_NOT_VERIFIED_EMAIL = "account.not.verified@test.it"

    const val TEST_ACCOUNT_NOT_VERIFIED_PASSWORD = "Test123!"

    const val TEST_ACCOUNT_CHANGE_PASS_EMAIL = "floraleye.change@gmail.com"

    const val TEST_ACCOUNT_CHANGE_PASS_PASSWORD_1 = "Floraleye2023!"

    const val TEST_ACCOUNT_CHANGE_PASS_PASSWORD_2 = "Floraleye2023?"

    const val TEST_ACCOUNT_WRONG_PASSWORD = "Password123!"

    const val NOT_EXISTING_USER = "test@test.it"

    const val MALFORMED_EMAIL = "email"

    const val WEAK_PASSWORD = "password"

    const val WIFI_DISABLE = "svc wifi disable"

    const val WIFI_ENABLE = "svc wifi enable"

    const val DATA_DISABLE = "svc data disable"

    const val DATA_ENABLE = "svc data enable"

    // *** inizio regione - Quiz

    const val SUBMIT = "Submit"

    const val NEXT = "Next"

    const val BORDER_TINT_MATCH = "border tint list should match "

    const val BORDER_TINT_NOT_MATCH = "border tint list should not match "

    // *** fine regione - Quiz

}
